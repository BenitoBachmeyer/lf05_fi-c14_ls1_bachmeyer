
public class Aufgabe_2 {

	public static void main(String[] args) {
		
		//Hier werden die Werte angegeben
		double zahl_1 = 2.36;
		double zahl_2 = 7.87;
		
		//Hier wird das Ergebnis festgeschrieben und die Werte an die Methode �bergeben
		double ergebnis = multiplikation(zahl_1, zahl_2);
		
		//Hier wird das Ergebnis als Ausgabe ausgegeben
		System.out.println(zahl_1 + " * " + zahl_2 + " = " + ergebnis);

	}
		//hier wird die Methode geschrieben
		static double multiplikation(double zahl_1, double zahl_2) {
			return zahl_1 * zahl_2;
		}
}
