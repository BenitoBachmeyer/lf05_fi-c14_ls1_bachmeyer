import java.util.Scanner;
import java.lang.Math.*;

public class Aufgabe_4 {

	public static void main(String[] args) {
     Scanner myScanner = new Scanner(System.in);
     System.out.print("Geben Sie an, welche Form sie berechnet haben wollen\nW f�r W�rfel,\nQ f�r Quader,\nP f�r Pyramide\noder K f�r Kugel.: ");
     String form_1 = myScanner.nextLine();
     
     //Hier wird das Volumen des W�rfels Ausgegeben
     if (form_1.equals("W")){
    	 System.out.print("\nBitte geben Sie a in Centimetern an: ");
    
         double a_1 = myScanner.nextInt();
         double ergebnis_1 = volumen_wuerfel(a_1);
         System.out.printf("\nIhr W�rfel hat ein Volumen von %.2f cm�." , ergebnis_1);
     }
    
     //hier wird das Volumen des Quaders ausgegeben
     else if (form_1.equals("Q")) {
    	 System.out.print("\nBitte geben Sie a in Centimetern an: ");
    	 double a_1 = myScanner.nextDouble();
    	 
    	 System.out.print("\nBitte geben Sie b in Centimetern an: ");
    	 double b_1 = myScanner.nextDouble();
    	 
    	 System.out.print("\nBitte geben Sie c in Centimetern an: ");
    	 double c_1 = myScanner.nextDouble();
    	 
    	 double ergebnis_1 = volumen_quader(a_1, b_1, c_1);
    	 System.out.printf("\nIhr Quader hat ein Volumen von %.2f cm�." , ergebnis_1);
     }
     
     else if (form_1.equals("P")) {
    	 
    	 System.out.print("\nBitte geben Sie a in Centimetern an: ");
    	 double a_1 = myScanner.nextDouble();
    	 
    	 System.out.print("\nBitte geben Sie die H�he h ind Centimetern an: ");
    	 double h_1 = myScanner.nextDouble();
    	 
    	 double ergebnis_1 = volumen_pyramide(a_1, h_1);
    	 
    	 System.out.printf("Das Volumen der Pyramide betr�gt %.2f cm�.", ergebnis_1);
     }
     
     else if (form_1.equals("K")) {
    	 System.out.print("\nBitte geben Sie den Radius r in Centimeter an: ");
    	 double r_1 = myScanner.nextDouble();
    	 double ergebnis_1 = volumen_kugel(r_1);
    	 
    	 System.out.printf("Das Volumen der Kugel betr�gt %.2f cm�." , ergebnis_1);
     }
     
     
     

     

	}
	//Hier wird das Volumen des W�rfels berechnet
	static double volumen_wuerfel(double a_1) {
		return a_1 * a_1 * a_1;
	}
	//hier wird das Volumen des Quaders berechnet
	static double volumen_quader(double a_1, double b_1, double c_1) {
		return a_1 * b_1 * c_1;
	}
	
	//hier wird das Volumen der Pyramide berechnet
	static double volumen_pyramide(double a_1, double h_1) {
		return a_1 * a_1 * h_1 / 3;

	}
	
	static double volumen_kugel(double r_1) {
		return 4 / 3 * r_1 * r_1 * r_1 * Math.PI;
		
	}
}	
		
		
	
