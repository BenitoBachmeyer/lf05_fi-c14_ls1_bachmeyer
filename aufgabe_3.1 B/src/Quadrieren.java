public class Quadrieren {
   
	public static void main(String[] args) {

		// (E) "Eingabe"
		// Wert f�r x festlegen:
		// ===========================
		System.out.println("Dieses Programm berechnet die Quadratzahl x�");
		System.out.println("---------------------------------------------");
		double x = 7;
		double ergebnis = quadrier_rechnung(x);

		// (A) Ausgabe
		// Ergebnis auf der Konsole ausgeben:
		// =================================
		System.out.printf("x = %.2f und x�= %.2f\n", x, ergebnis);

	}
	
	static double quadrier_rechnung(double x) {
		return x*x;
	}
}
