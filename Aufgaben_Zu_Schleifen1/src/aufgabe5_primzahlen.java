
public class aufgabe5_primzahlen {
	public static void main(String[] args) {
		int maxNum = 100, temp;
		
		for(int i = 2; i < maxNum; i++) {
			for(int j = 2; j < i; j++) {
				if(i%j == 0)
					break;
				
				if(j == i - 1)
					System.out.printf("%d, ", i);
			}
		}
	}
}