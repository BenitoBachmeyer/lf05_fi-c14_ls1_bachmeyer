
public class Mittelwert {

   public static void main(String[] args) {

      // (E) "Eingabe"
      // Werte f�r x und y festlegen:
      // ===========================
      double x = 1.0;
      double y = 7.5;
      double m = mittelwert_rechnung(x, y);
      
      
      System.out.printf("Das Mittelwert von %.2f und %.2f ist %.2f\n", x, y, m);
 
   }
   
   static double mittelwert_rechnung(double x, double y) {
	   return (x+y) / 2.0;
   }
}
