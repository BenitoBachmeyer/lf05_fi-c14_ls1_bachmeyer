import java.util.Scanner;

public class AB_1 {
	
	public static void main(String[] args) {

		Scanner meinScanner = new Scanner(System.in)
				;
		System.out.print("Bitte geben Sie die erste Zahl ein: ");
		
		int zahl_1 = meinScanner.nextInt();
		
		System.out.print("\nBitte geben Sie die zweite Zahl ein: ");
		int zahl_2 = meinScanner.nextInt();
		
		if (zahl_1 == zahl_2) {
			System.out.println("Die erste und die zweite Zahl sind gleich gro�!");
		}
		
		else if (zahl_1 < zahl_2) {
			System.out.println("Zahl 2 ist gr��er als Zahl 1");
		}
		
		else if (zahl_1 >= zahl_2) {
			System.out.println("Zahl 1 ist gr��er gleich Zahl 2!");
		}
	
		else {
			
			System.out.println("Es liegt ein Fehler vor, Programm bitte neustarten!");
		}
	}

}
