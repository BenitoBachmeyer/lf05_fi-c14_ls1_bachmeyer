import java.util.Scanner;

public class aufgabe_6 {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie x ein: ");
		double wert_x = Scanner_BB.nextDouble();
		
		double ergebnis;
		
		if (wert_x <= 0) {
			double eulersche_zahl = 2.718;
			
			ergebnis = Math.pow(eulersche_zahl, wert_x);
			System.out.print("Der Funktionswert f(x) lautet " + ergebnis + ". Der Bereich des Wertes ist exponentiell.");
			
			
		}
		else if (wert_x > 0 && wert_x <= 3) {
			ergebnis = Math.pow(wert_x, 2) + 1;
			
			System.out.print("Der Funktionswert f(x) lautet " + ergebnis + ". Der Bereich des Wertes ist quadratisch");
		}
		else if (wert_x > 3) {
			ergebnis = 2 * wert_x + 4;
			System.out.print("Der Funktionswert f(x) lautet " + ergebnis + ". Der Bereich des Wertes ist linear.");
		}
		Scanner_BB.close();
	}
}
