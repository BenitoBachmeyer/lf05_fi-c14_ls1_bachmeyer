import java.util.Scanner;

public class aufgabe_zu_verzweigungen_Aufgabe3 {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Wie hoch ist der Einzelpreis einer Maus?: ");
		double einzelpreis_maus = Scanner_BB.nextDouble();
		
		System.out.print("\nWie viele M�use werden bestellt?: ");
		int maus_anzahl = Scanner_BB.nextInt();
		
		if (maus_anzahl >= 10) {
			double rechnungsbetrag = einzelpreis_maus * maus_anzahl * 1.19;
			
			System.out.print("\nDie Rechnung betr�gt " + rechnungsbetrag +"�");
		}
		
		else if (maus_anzahl < 10) {
			double rechnungsbetrag = (einzelpreis_maus * maus_anzahl + 10) * 1.19;
			
			System.out.print("\nDie Rechnung betr�gt " + rechnungsbetrag +"�");
		}
		
		Scanner_BB.close();
	}
}
