import java.util.Scanner;

public class aufgabe_5 {
	public static void main(String[] args) {
		
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie ihre Körpergröße in Meter an: ");
		double körpergröße = Scanner_BB.nextDouble();
		
		System.out.print("Bitte geben Sie ihr Gewicht in Kilogramm an: ");
		double gewicht = Scanner_BB.nextDouble();
		
		System.out.print("Geben Sie bitte ihr Geschlecht an. m für männlich oder w für weiblich.: ");
		char geschlecht = Scanner_BB.next().charAt(0);
		
		double körpergröße_quadrat = körpergröße * körpergröße;
		
		double bmi_1 = gewicht / körpergröße_quadrat;
		
		if (geschlecht == 'm' && bmi_1 < 20) {
			System.out.print("\nSie sind untergewichtig.");
		}
		else if (geschlecht == 'w' && bmi_1 < 19) {
			System.out.print("\nSie sind untergewichtig.");
		}
		else if (geschlecht == 'm' && bmi_1 >= 20 && bmi_1 <=25) {
			System.out.print("Sie haben ein normales Gewicht");
		}
		
		else if (geschlecht == 'w' && bmi_1 >= 19 && bmi_1 <= 24) {
			System.out.print("Sie haben ein normales Gewicht");
		}
		
		else if (geschlecht == 'm' && bmi_1 > 25) {
			System.out.print("Sie sind übergewichtig");
			
		}
		
		else if (geschlecht == 'w' && bmi_1 > 24) {
			System.out.print("Sie sind übergewichtig");
		}
		Scanner_BB.close();
	}
}
