import java.util.Scanner;

public class aufgabe_zu_verzweigungen_aufgabe4 {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie den Bestellwert in Euro an: ");
		double bestellwert_1 = Scanner_BB.nextDouble();
		
		double endpreis_1;
		if (bestellwert_1 <= 100) {
			endpreis_1 = bestellwert_1 * 0.9 * 1.19;
			System.out.print("Der reduzierte Preis betr�gt " + endpreis_1 + "�");
		}
		else if (bestellwert_1 > 100 && bestellwert_1 <= 500) {
			endpreis_1 = bestellwert_1 * 0.85 * 1.19;
			System.out.print("Der reduzierte Preis betr�gt " + endpreis_1 + "�");
		}
		
		else if (bestellwert_1 > 500) {
			endpreis_1 = bestellwert_1 * 0.8 * 1.19;
			System.out.print("Der reduzierte Preis betr�gt " + endpreis_1 + "�");
		}
		Scanner_BB.close();
	}
}
