import java.util.Scanner;

public class verzweigungen_aufgabe2 {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie den Nettobetrag ein: ");
		double nettobetrag = Scanner_BB.nextFloat();
		
		System.out.print("Bitte w�hlen sie aus, ob der erm��igte oder volle Steuersatz angewendet werden soll. Geben Sie \"j\" f�r den erm��igten, und \"n\" f�r den vollen Steuersatz an: ");
		char steuersatz_1 = Scanner_BB.next().charAt(0);
		
		while (steuersatz_1 != 'j' && steuersatz_1 != 'n') {
			System.out.print("Falsche Eingabe f�r den Steuersatz. Bitte erneut eingeben (Nur \"j\n oder \"n\" sind m�glich Eingaben.");
			steuersatz_1 = Scanner_BB.next().charAt(0);
		}
		if (steuersatz_1 == 'j') {
			double bruttobetrag_1 = nettobetrag * 1.07;
			System.out.print("\nDer Bruttobetrag betr�gt " + bruttobetrag_1);
			
		}
		
		else if (steuersatz_1 == 'n') {
			double bruttobetrag_1 = nettobetrag * 1.19;
			System.out.print("Der Bruttobetrag betr�gt " + bruttobetrag_1);
		}
		Scanner_BB.close();
	}

}
