package aufgabenzuschleifen2;

import java.util.Scanner;

public class aufgabe3 {
	public static void main (String [] args) {
		
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Geben Sie bitte eine Zahl ein: ");
		
		int z = Scanner_BB.nextInt();
		int ergebnis1=0;
		
		while(z!=0) {
			ergebnis1 = ergebnis1 + ( z % 10); // +=
			z/=10;
		}
		
		System.out.print(ergebnis1);
	}
}
