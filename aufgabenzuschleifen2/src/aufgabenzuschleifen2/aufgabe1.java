package aufgabenzuschleifen2;
import java.util.Scanner;

public class aufgabe1 {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Bitte geben Sie n an: ");
		
		int n= Scanner_BB.nextInt();
		
		//a)
		int i=1;
		
		while(i<=n) {
			System.out.print(i+",");
			i++;
		}
	
		//b)
		System.out.print("\n\n");
		while(n>0) {
			System.out.print(n+",");
			
			n--;
		}
	
	
	}
}
