package aufgabenblatt_fallunterscheidungen;
import java.util.Scanner;

public class aufgabe_ohmsches_gesetz {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.print("Bitte Geben Sie ein, welche physikalische Gr��e berechnet werden soll. R f�r Widerstand, U f�r Spannung oder I f�r Stromst�rke.: ");
		
		char groe�e1 = Scanner_BB.next().charAt(0);
		
		if (groe�e1 =='r'||groe�e1=='R') {
			System.out.print("\nBitte geben Sie die Spannung in Volt an: ");
			float spannung1 = Scanner_BB.nextFloat();
			
			System.out.print("\nBitte geben Sie die Stromst�rke in Amper an: ");
			float stromstark1 = Scanner_BB.nextFloat();
			
			float ergebnis1 = spannung1 / stromstark1;
			System.out.print("Der Widerstand betr�gt " +ergebnis1+ " Ohm.");
		}
		else if (groe�e1 =='U'||groe�e1=='u') {
			System.out.print("Bitte geben Sie den Widerstand in Ohm an.: ");
			float widerstand1 = Scanner_BB.nextFloat();
			
			System.out.print("Bitte geben Sie die Stromst�rke in Amper an.: ");
			float stromstark1 = Scanner_BB.nextFloat();
			
			float ergebnis1= widerstand1 * stromstark1;
			
			System.out.print("Die Spannung betr�gt " +ergebnis1+ " Volt.");
		}
		else if (groe�e1 =='A'||groe�e1=='a') {
			System.out.print("Bitte geben Sie die Spannung in Volt an.: ");
			float spannung1 = Scanner_BB.nextFloat();
			
			System.out.print("Bitte geben Sie den Widerstand in Ohm an.: ");
			float widerstand1 = Scanner_BB.nextFloat();
			
			float ergebnis1 = spannung1 / widerstand1;
			
			System.out.print("Die Stromst�rke betr�gt " +ergebnis1+ " Amper.");
		}
		else {
			System.out.print("Falsche Eingabe. Bitte Programm neustarten und m�gliche Gr��e angeben.");
		}
	}
}
