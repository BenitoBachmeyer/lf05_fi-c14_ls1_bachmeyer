package aufgabenblatt_fallunterscheidungen;
import java.util.Scanner;

public class roemische_zahlen {
	public static void main(String[] args) {
		Scanner Scanner_BB = new Scanner(System.in);
		
		System.out.println("Bitte geben Sie ihre r�mische Zahl ein: ");
		char zahl1 = Scanner_BB.next().charAt(0);
		
		if(zahl1 == 'i' || zahl1 == 'I') {
			System.out.println("Ihre Zahl ist 1.");
		}
		else if(zahl1 == 'V' || zahl1 == 'v') {
			System.out.println("Ihre Zahl ist 5.");
		}
		else if (zahl1 == 'x'||zahl1=='X') {
			System.out.println("Ihre Zahl ist 10.");
		}
		else if(zahl1=='L'||zahl1=='l') {
			System.out.println("Ihre Zahl ist 50.");
		}
		else if(zahl1=='C'||zahl1=='c') {
			System.out.println("Ihre Zahl ist 100.");
		}
		else if(zahl1=='D'||zahl1=='d') {
			System.out.println("Ihre Zahl ist 500.");
		}
		else if(zahl1=='M'||zahl1=='m') {
			System.out.println("Ihre Zahl ist 1000.");
		}
		else {
			System.out.print("Falscher Wert. Bitte Programm neu starten und korrekte Zahl eingebn.");
		}
		Scanner_BB.close();
	}
}
